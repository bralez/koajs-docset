<html><head></head><body><a class="dashingAutolink" name="autolink-1"></a><a class="dashAnchor" name="//apple_ref/cpp/Section/Installation"></a><h1 id="installation">Installation</h1>
<p>Koa requires <strong>node v7.6.0</strong> or higher for ES2015 and async function support.</p>
<p>You can quickly install a supported version of node with your favorite version manager:</p>
<div class="sourceCode" id="cb1"><pre class="sourceCode bash"><code class="sourceCode bash"><a class="sourceLine" id="cb1-1" data-line-number="1">$ <span class="ex">nvm</span> install 7</a>
<a class="sourceLine" id="cb1-2" data-line-number="2">$ <span class="ex">npm</span> i koa</a>
<a class="sourceLine" id="cb1-3" data-line-number="3">$ <span class="ex">node</span> my-koa-app.js</a></code></pre></div>
<h2 id="async-functions-with-babel">Async Functions with Babel</h2>
<p>To use <code>async</code> functions in Koa in versions of node &lt; 7.6, we recommend using <a href="http://babeljs.io/docs/usage/babel-register/">babel&#39;s require hook</a>.</p>
<div class="sourceCode" id="cb2"><pre class="sourceCode js"><code class="sourceCode javascript"><a class="sourceLine" id="cb2-1" data-line-number="1"><span class="at">require</span>(<span class="st">&#39;babel-register&#39;</span>)<span class="op">;</span></a>
<a class="sourceLine" id="cb2-2" data-line-number="2"><span class="co">// require the rest of the app that needs to be transpiled after the hook</span></a>
<a class="sourceLine" id="cb2-3" data-line-number="3"><span class="kw">const</span> app <span class="op">=</span> <span class="at">require</span>(<span class="st">&#39;./app&#39;</span>)<span class="op">;</span></a></code></pre></div>
<p>To parse and transpile async functions, you should at a minimum have the <a href="http://babeljs.io/docs/plugins/transform-async-to-generator/">transform-async-to-generator</a> or <a href="http://babeljs.io/docs/plugins/transform-async-to-module-method/">transform-async-to-module-method</a> plugins. For example, in your <code>.babelrc</code> file, you should have:</p>
<div class="sourceCode" id="cb3"><pre class="sourceCode json"><code class="sourceCode json"><a class="sourceLine" id="cb3-1" data-line-number="1"><span class="fu">{</span></a>
<a class="sourceLine" id="cb3-2" data-line-number="2">  <span class="dt">&#34;plugins&#34;</span><span class="fu">:</span> <span class="ot">[</span><span class="st">&#34;transform-async-to-generator&#34;</span><span class="ot">]</span></a>
<a class="sourceLine" id="cb3-3" data-line-number="3"><span class="fu">}</span></a></code></pre></div>
<p>You can also use the <a href="http://babeljs.io/docs/plugins/preset-env/">env preset</a> with a target option <code>&#34;node&#34;: &#34;current&#34;</code> instead.</p>
<a class="dashingAutolink" name="autolink-2"></a><a class="dashAnchor" name="//apple_ref/cpp/Section/Application"></a><h1 id="application">Application</h1>
<p>A Koa application is an object containing an array of middleware functions which are composed and executed in a stack-like manner upon request. Koa is similar to many other middleware systems that you may have encountered such as Ruby&#39;s Rack, Connect, and so on - however a key design decision was made to provide high level &#34;sugar&#34; at the otherwise low-level middleware layer. This improves interoperability, robustness, and makes writing middleware much more enjoyable.</p>
<p>This includes methods for common tasks like content-negotiation, cache freshness, proxy support, and redirection among others. Despite supplying a reasonably large number of helpful methods Koa maintains a small footprint, as no middleware are bundled.</p>
<p>The obligatory hello world application:</p>
<div class="sourceCode" id="cb4"><pre class="sourceCode js"><code class="sourceCode javascript"><a class="sourceLine" id="cb4-1" data-line-number="1"><span class="kw">const</span> Koa <span class="op">=</span> <span class="at">require</span>(<span class="st">&#39;koa&#39;</span>)<span class="op">;</span></a>
<a class="sourceLine" id="cb4-2" data-line-number="2"><span class="kw">const</span> app <span class="op">=</span> <span class="kw">new</span> <span class="at">Koa</span>()<span class="op">;</span></a>
<a class="sourceLine" id="cb4-3" data-line-number="3"></a>
<a class="sourceLine" id="cb4-4" data-line-number="4"><span class="va">app</span>.<span class="at">use</span>(async ctx <span class="op">=&gt;</span> <span class="op">{</span></a>
<a class="sourceLine" id="cb4-5" data-line-number="5">  <span class="va">ctx</span>.<span class="at">body</span> <span class="op">=</span> <span class="st">&#39;Hello World&#39;</span><span class="op">;</span></a>
<a class="sourceLine" id="cb4-6" data-line-number="6"><span class="op">}</span>)<span class="op">;</span></a>
<a class="sourceLine" id="cb4-7" data-line-number="7"></a>
<a class="sourceLine" id="cb4-8" data-line-number="8"><span class="va">app</span>.<span class="at">listen</span>(<span class="dv">3000</span>)<span class="op">;</span></a></code></pre></div>
<h2 id="cascading">Cascading</h2>
<p>Koa middleware cascade in a more traditional way as you may be used to with similar tools - this was previously difficult to make user friendly with node&#39;s use of callbacks. However with async functions we can achieve &#34;true&#34; middleware. Contrasting Connect&#39;s implementation which simply passes control through series of functions until one returns, Koa invoke &#34;downstream&#34;, then control flows back &#34;upstream&#34;.</p>
<p>The following example responds with &#34;Hello World&#34;, however first the request flows through the <code>x-response-time</code> and <code>logging</code> middleware to mark when the request started, then continue to yield control through the response middleware. When a middleware invokes <code>next()</code> the function suspends and passes control to the next middleware defined. After there are no more middleware to execute downstream, the stack will unwind and each middleware is resumed to perform its upstream behaviour.</p>
<div class="sourceCode" id="cb5"><pre class="sourceCode js"><code class="sourceCode javascript"><a class="sourceLine" id="cb5-1" data-line-number="1"><span class="kw">const</span> Koa <span class="op">=</span> <span class="at">require</span>(<span class="st">&#39;koa&#39;</span>)<span class="op">;</span></a>
<a class="sourceLine" id="cb5-2" data-line-number="2"><span class="kw">const</span> app <span class="op">=</span> <span class="kw">new</span> <span class="at">Koa</span>()<span class="op">;</span></a>
<a class="sourceLine" id="cb5-3" data-line-number="3"></a>
<a class="sourceLine" id="cb5-4" data-line-number="4"><span class="co">// x-response-time</span></a>
<a class="sourceLine" id="cb5-5" data-line-number="5"></a>
<a class="sourceLine" id="cb5-6" data-line-number="6"><span class="va">app</span>.<span class="at">use</span>(<span class="at">async</span> (ctx<span class="op">,</span> next) <span class="op">=&gt;</span> <span class="op">{</span></a>
<a class="sourceLine" id="cb5-7" data-line-number="7">  <span class="kw">const</span> start <span class="op">=</span> <span class="va">Date</span>.<span class="at">now</span>()<span class="op">;</span></a>
<a class="sourceLine" id="cb5-8" data-line-number="8">  await <span class="at">next</span>()<span class="op">;</span></a>
<a class="sourceLine" id="cb5-9" data-line-number="9">  <span class="kw">const</span> ms <span class="op">=</span> <span class="va">Date</span>.<span class="at">now</span>() <span class="op">-</span> start<span class="op">;</span></a>
<a class="sourceLine" id="cb5-10" data-line-number="10">  <span class="va">ctx</span>.<span class="at">set</span>(<span class="st">&#39;X-Response-Time&#39;</span><span class="op">,</span> <span class="vs">`</span><span class="sc">${</span>ms<span class="sc">}</span><span class="vs">ms`</span>)<span class="op">;</span></a>
<a class="sourceLine" id="cb5-11" data-line-number="11"><span class="op">}</span>)<span class="op">;</span></a>
<a class="sourceLine" id="cb5-12" data-line-number="12"></a>
<a class="sourceLine" id="cb5-13" data-line-number="13"><span class="co">// logger</span></a>
<a class="sourceLine" id="cb5-14" data-line-number="14"></a>
<a class="sourceLine" id="cb5-15" data-line-number="15"><span class="va">app</span>.<span class="at">use</span>(<span class="at">async</span> (ctx<span class="op">,</span> next) <span class="op">=&gt;</span> <span class="op">{</span></a>
<a class="sourceLine" id="cb5-16" data-line-number="16">  <span class="kw">const</span> start <span class="op">=</span> <span class="va">Date</span>.<span class="at">now</span>()<span class="op">;</span></a>
<a class="sourceLine" id="cb5-17" data-line-number="17">  await <span class="at">next</span>()<span class="op">;</span></a>
<a class="sourceLine" id="cb5-18" data-line-number="18">  <span class="kw">const</span> ms <span class="op">=</span> <span class="va">Date</span>.<span class="at">now</span>() <span class="op">-</span> start<span class="op">;</span></a>
<a class="sourceLine" id="cb5-19" data-line-number="19">  <span class="va">console</span>.<span class="at">log</span>(<span class="vs">`</span><span class="sc">${</span><span class="va">ctx</span>.<span class="at">method</span><span class="sc">}</span><span class="vs"> </span><span class="sc">${</span><span class="va">ctx</span>.<span class="at">url</span><span class="sc">}</span><span class="vs"> - </span><span class="sc">${</span>ms<span class="sc">}</span><span class="vs">`</span>)<span class="op">;</span></a>
<a class="sourceLine" id="cb5-20" data-line-number="20"><span class="op">}</span>)<span class="op">;</span></a>
<a class="sourceLine" id="cb5-21" data-line-number="21"></a>
<a class="sourceLine" id="cb5-22" data-line-number="22"><span class="co">// response</span></a>
<a class="sourceLine" id="cb5-23" data-line-number="23"></a>
<a class="sourceLine" id="cb5-24" data-line-number="24"><span class="va">app</span>.<span class="at">use</span>(async ctx <span class="op">=&gt;</span> <span class="op">{</span></a>
<a class="sourceLine" id="cb5-25" data-line-number="25">  <span class="va">ctx</span>.<span class="at">body</span> <span class="op">=</span> <span class="st">&#39;Hello World&#39;</span><span class="op">;</span></a>
<a class="sourceLine" id="cb5-26" data-line-number="26"><span class="op">}</span>)<span class="op">;</span></a>
<a class="sourceLine" id="cb5-27" data-line-number="27"></a>
<a class="sourceLine" id="cb5-28" data-line-number="28"><span class="va">app</span>.<span class="at">listen</span>(<span class="dv">3000</span>)<span class="op">;</span></a></code></pre></div>
<h2 id="settings">Settings</h2>
<p>Application settings are properties on the <code>app</code> instance, currently the following are supported:</p>
<ul>
<li><code>app.env</code> defaulting to the <strong>NODE_ENV</strong> or &#34;development&#34;</li>
<li><code>app.proxy</code> when true proxy header fields will be trusted</li>
<li><code>app.subdomainOffset</code> offset of <code>.subdomains</code> to ignore [2]</li>
</ul>
<h2 id="applisten">app.listen(...)</h2>
<p>A Koa application is not a 1-to-1 representation of an HTTP server. One or more Koa applications may be mounted together to form larger applications with a single HTTP server.</p>
<p>Create and return an HTTP server, passing the given arguments to <code>Server#listen()</code>. These arguments are documented on <a href="http://nodejs.org/api/http.html#http_server_listen_port_hostname_backlog_callback">nodejs.org</a>. The following is a useless Koa application bound to port <code>3000</code>:</p>
<div class="sourceCode" id="cb6"><pre class="sourceCode js"><code class="sourceCode javascript"><a class="sourceLine" id="cb6-1" data-line-number="1"><span class="kw">const</span> Koa <span class="op">=</span> <span class="at">require</span>(<span class="st">&#39;koa&#39;</span>)<span class="op">;</span></a>
<a class="sourceLine" id="cb6-2" data-line-number="2"><span class="kw">const</span> app <span class="op">=</span> <span class="kw">new</span> <span class="at">Koa</span>()<span class="op">;</span></a>
<a class="sourceLine" id="cb6-3" data-line-number="3"><span class="va">app</span>.<span class="at">listen</span>(<span class="dv">3000</span>)<span class="op">;</span></a></code></pre></div>
<p>The <code>app.listen(...)</code> method is simply sugar for the following:</p>
<div class="sourceCode" id="cb7"><pre class="sourceCode js"><code class="sourceCode javascript"><a class="sourceLine" id="cb7-1" data-line-number="1"><span class="kw">const</span> http <span class="op">=</span> <span class="at">require</span>(<span class="st">&#39;http&#39;</span>)<span class="op">;</span></a>
<a class="sourceLine" id="cb7-2" data-line-number="2"><span class="kw">const</span> Koa <span class="op">=</span> <span class="at">require</span>(<span class="st">&#39;koa&#39;</span>)<span class="op">;</span></a>
<a class="sourceLine" id="cb7-3" data-line-number="3"><span class="kw">const</span> app <span class="op">=</span> <span class="kw">new</span> <span class="at">Koa</span>()<span class="op">;</span></a>
<a class="sourceLine" id="cb7-4" data-line-number="4"><span class="va">http</span>.<span class="at">createServer</span>(<span class="va">app</span>.<span class="at">callback</span>()).<span class="at">listen</span>(<span class="dv">3000</span>)<span class="op">;</span></a></code></pre></div>
<p>This means you can spin up the same application as both HTTP and HTTPS or on multiple addresses:</p>
<div class="sourceCode" id="cb8"><pre class="sourceCode js"><code class="sourceCode javascript"><a class="sourceLine" id="cb8-1" data-line-number="1"><span class="kw">const</span> http <span class="op">=</span> <span class="at">require</span>(<span class="st">&#39;http&#39;</span>)<span class="op">;</span></a>
<a class="sourceLine" id="cb8-2" data-line-number="2"><span class="kw">const</span> https <span class="op">=</span> <span class="at">require</span>(<span class="st">&#39;https&#39;</span>)<span class="op">;</span></a>
<a class="sourceLine" id="cb8-3" data-line-number="3"><span class="kw">const</span> Koa <span class="op">=</span> <span class="at">require</span>(<span class="st">&#39;koa&#39;</span>)<span class="op">;</span></a>
<a class="sourceLine" id="cb8-4" data-line-number="4"><span class="kw">const</span> app <span class="op">=</span> <span class="kw">new</span> <span class="at">Koa</span>()<span class="op">;</span></a>
<a class="sourceLine" id="cb8-5" data-line-number="5"><span class="va">http</span>.<span class="at">createServer</span>(<span class="va">app</span>.<span class="at">callback</span>()).<span class="at">listen</span>(<span class="dv">3000</span>)<span class="op">;</span></a>
<a class="sourceLine" id="cb8-6" data-line-number="6"><span class="va">https</span>.<span class="at">createServer</span>(<span class="va">app</span>.<span class="at">callback</span>()).<span class="at">listen</span>(<span class="dv">3001</span>)<span class="op">;</span></a></code></pre></div>
<h2 id="appcallback">app.callback()</h2>
<p>Return a callback function suitable for the <code>http.createServer()</code> method to handle a request. You may also use this callback function to mount your Koa app in a Connect/Express app.</p>
<h2 id="appusefunction">app.use(function)</h2>
<p>Add the given middleware function to this application. See <a href="https://github.com/koajs/koa/wiki#middleware">Middleware</a> for more information.</p>
<h2 id="appkeys">app.keys=</h2>
<p>Set signed cookie keys.</p>
<p>These are passed to <a href="https://github.com/jed/keygrip">KeyGrip</a>, however you may also pass your own <code>KeyGrip</code> instance. For example the following are acceptable:</p>
<div class="sourceCode" id="cb9"><pre class="sourceCode js"><code class="sourceCode javascript"><a class="sourceLine" id="cb9-1" data-line-number="1"><span class="va">app</span>.<span class="at">keys</span> <span class="op">=</span> [<span class="st">&#39;im a newer secret&#39;</span><span class="op">,</span> <span class="st">&#39;i like turtle&#39;</span>]<span class="op">;</span></a>
<a class="sourceLine" id="cb9-2" data-line-number="2"><span class="va">app</span>.<span class="at">keys</span> <span class="op">=</span> <span class="kw">new</span> <span class="at">KeyGrip</span>([<span class="st">&#39;im a newer secret&#39;</span><span class="op">,</span> <span class="st">&#39;i like turtle&#39;</span>]<span class="op">,</span> <span class="st">&#39;sha256&#39;</span>)<span class="op">;</span></a></code></pre></div>
<p>These keys may be rotated and are used when signing cookies with the <code>{ signed: true }</code> option:</p>
<div class="sourceCode" id="cb10"><pre class="sourceCode js"><code class="sourceCode javascript"><a class="sourceLine" id="cb10-1" data-line-number="1"><span class="va">ctx</span>.<span class="va">cookies</span>.<span class="at">set</span>(<span class="st">&#39;name&#39;</span><span class="op">,</span> <span class="st">&#39;tobi&#39;</span><span class="op">,</span> <span class="op">{</span> <span class="dt">signed</span><span class="op">:</span> <span class="kw">true</span> <span class="op">}</span>)<span class="op">;</span></a></code></pre></div>
<h2 id="appcontext">app.context</h2>
<p><code>app.context</code> is the prototype from which <code>ctx</code> is created. You may add additional properties to <code>ctx</code> by editing <code>app.context</code>. This is useful for adding properties or methods to <code>ctx</code> to be used across your entire app, which may be more performant (no middleware) and/or easier (fewer <code>require()</code>s) at the expense of relying more on <code>ctx</code>, which could be considered an anti-pattern.</p>
<p>For example, to add a reference to your database from <code>ctx</code>:</p>
<div class="sourceCode" id="cb11"><pre class="sourceCode js"><code class="sourceCode javascript"><a class="sourceLine" id="cb11-1" data-line-number="1"><span class="va">app</span>.<span class="va">context</span>.<span class="at">db</span> <span class="op">=</span> <span class="at">db</span>()<span class="op">;</span></a>
<a class="sourceLine" id="cb11-2" data-line-number="2"></a>
<a class="sourceLine" id="cb11-3" data-line-number="3"><span class="va">app</span>.<span class="at">use</span>(async ctx <span class="op">=&gt;</span> <span class="op">{</span></a>
<a class="sourceLine" id="cb11-4" data-line-number="4">  <span class="va">console</span>.<span class="at">log</span>(<span class="va">ctx</span>.<span class="at">db</span>)<span class="op">;</span></a>
<a class="sourceLine" id="cb11-5" data-line-number="5"><span class="op">}</span>)<span class="op">;</span></a></code></pre></div>
<p>Note:</p>
<ul>
<li>Many properties on <code>ctx</code> are defined using getters, setters, and <code>Object.defineProperty()</code>. You can only edit these properties (not recommended) by using <code>Object.defineProperty()</code> on <code>app.context</code>. See <a href="https://github.com/koajs/koa/issues/652" class="uri">https://github.com/koajs/koa/issues/652</a>.</li>
<li>Mounted apps currently use its parent&#39;s <code>ctx</code> and settings. Thus, mounted apps are really just groups of middleware.</li>
</ul>
<h2 id="error-handling">Error Handling</h2>
<p>By default outputs all errors to stderr unless <code>app.silent</code> is <code>true</code>. The default error handler also won&#39;t output errors when <code>err.status</code> is <code>404</code> or <code>err.expose</code> is <code>true</code>. To perform custom error-handling logic such as centralized logging you can add an &#34;error&#34; event listener:</p>
<div class="sourceCode" id="cb12"><pre class="sourceCode js"><code class="sourceCode javascript"><a class="sourceLine" id="cb12-1" data-line-number="1"><span class="va">app</span>.<span class="at">on</span>(<span class="st">&#39;error&#39;</span><span class="op">,</span> err <span class="op">=&gt;</span> <span class="op">{</span></a>
<a class="sourceLine" id="cb12-2" data-line-number="2">  <span class="va">log</span>.<span class="at">error</span>(<span class="st">&#39;server error&#39;</span><span class="op">,</span> err)</a>
<a class="sourceLine" id="cb12-3" data-line-number="3"><span class="op">}</span>)<span class="op">;</span></a></code></pre></div>
<p>If an error is in the req/res cycle and it is <em>not</em> possible to respond to the client, the <code>Context</code> instance is also passed:</p>
<div class="sourceCode" id="cb13"><pre class="sourceCode js"><code class="sourceCode javascript"><a class="sourceLine" id="cb13-1" data-line-number="1"><span class="va">app</span>.<span class="at">on</span>(<span class="st">&#39;error&#39;</span><span class="op">,</span> (err<span class="op">,</span> ctx) <span class="op">=&gt;</span> <span class="op">{</span></a>
<a class="sourceLine" id="cb13-2" data-line-number="2">  <span class="va">log</span>.<span class="at">error</span>(<span class="st">&#39;server error&#39;</span><span class="op">,</span> err<span class="op">,</span> ctx)</a>
<a class="sourceLine" id="cb13-3" data-line-number="3"><span class="op">}</span>)<span class="op">;</span></a></code></pre></div>
<p>When an error occurs <em>and</em> it is still possible to respond to the client, aka no data has been written to the socket, Koa will respond appropriately with a 500 &#34;Internal Server Error&#34;. In either case an app-level &#34;error&#34; is emitted for logging purposes.</p>
</body></html>