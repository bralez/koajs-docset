<html><head></head><body><a class="dashingAutolink" name="autolink-0"></a><a class="dashAnchor" name="//apple_ref/cpp/Section/Context"></a><h1 id="context">Context</h1>
<p>A Koa Context encapsulates node&#39;s <code>request</code> and <code>response</code> objects into a single object which provides many helpful methods for writing web applications and APIs. These operations are used so frequently in HTTP server development that they are added at this level instead of a higher level framework, which would force middleware to re-implement this common functionality.</p>
<p>A <code>Context</code> is created <em>per</em> request, and is referenced in middleware as the receiver, or the <code>ctx</code> identifier, as shown in the following snippet:</p>
<div class="sourceCode" id="cb1"><pre class="sourceCode js"><code class="sourceCode javascript"><a class="sourceLine" id="cb1-1" data-line-number="1"><span class="va">app</span>.<span class="at">use</span>(async ctx <span class="op">=&gt;</span> <span class="op">{</span></a>
<a class="sourceLine" id="cb1-2" data-line-number="2">  ctx<span class="op">;</span> <span class="co">// is the Context</span></a>
<a class="sourceLine" id="cb1-3" data-line-number="3">  <span class="va">ctx</span>.<span class="at">request</span><span class="op">;</span> <span class="co">// is a Koa Request</span></a>
<a class="sourceLine" id="cb1-4" data-line-number="4">  <span class="va">ctx</span>.<span class="at">response</span><span class="op">;</span> <span class="co">// is a Koa Response</span></a>
<a class="sourceLine" id="cb1-5" data-line-number="5"><span class="op">}</span>)<span class="op">;</span></a></code></pre></div>
<p>Many of the context&#39;s accessors and methods simply delegate to their <code>ctx.request</code> or <code>ctx.response</code> equivalents for convenience, and are otherwise identical. For example <code>ctx.type</code> and <code>ctx.length</code> delegate to the <code>response</code> object, and <code>ctx.path</code> and <code>ctx.method</code> delegate to the <code>request</code>.</p>
<h2 id="api">API</h2>
<p><code>Context</code> specific methods and accessors.</p>
<h3 id="ctxreq">ctx.req</h3>
<p>Node&#39;s <code>request</code> object.</p>
<h3 id="ctxres">ctx.res</h3>
<p>Node&#39;s <code>response</code> object.</p>
<p>Bypassing Koa&#39;s response handling is <strong>not supported</strong>. Avoid using the following node properties:</p>
<ul>
<li><code>res.statusCode</code></li>
<li><code>res.writeHead()</code></li>
<li><code>res.write()</code></li>
<li><code>res.end()</code></li>
</ul>
<h3 id="ctxrequest">ctx.request</h3>
<p>A Koa <code>Request</code> object.</p>
<h3 id="ctxresponse">ctx.response</h3>
<p>A Koa <code>Response</code> object.</p>
<h3 id="ctxstate">ctx.state</h3>
<p>The recommended namespace for passing information through middleware and to your frontend views.</p>
<div class="sourceCode" id="cb2"><pre class="sourceCode js"><code class="sourceCode javascript"><a class="sourceLine" id="cb2-1" data-line-number="1"><span class="va">ctx</span>.<span class="va">state</span>.<span class="at">user</span> <span class="op">=</span> await <span class="va">User</span>.<span class="at">find</span>(id)<span class="op">;</span></a></code></pre></div>
<h3 id="ctxapp">ctx.app</h3>
<p>Application instance reference.</p>
<h3 id="ctxcookiesgetname-options">ctx.cookies.get(name, [options])</h3>
<p>Get cookie <code>name</code> with <code>options</code>:</p>
<ul>
<li><code>signed</code> the cookie requested should be signed</li>
</ul>
<p>Koa uses the <a href="https://github.com/jed/cookies">cookies</a> module where options are simply passed.</p>
<h3 id="ctxcookiessetname-value-options">ctx.cookies.set(name, value, [options])</h3>
<p>Set cookie <code>name</code> to <code>value</code> with <code>options</code>:</p>
<ul>
<li><code>maxAge</code> a number representing the milliseconds from Date.now() for expiry</li>
<li><code>signed</code> sign the cookie value</li>
<li><code>expires</code> a <code>Date</code> for cookie expiration</li>
<li><code>path</code> cookie path, <code>/&#39;</code> by default</li>
<li><code>domain</code> cookie domain</li>
<li><code>secure</code> secure cookie</li>
<li><code>httpOnly</code> server-accessible cookie, <strong>true</strong> by default</li>
<li><code>overwrite</code> a boolean indicating whether to overwrite previously set cookies of the same name (<strong>false</strong> by default). If this is true, all cookies set during the same request with the same name (regardless of path or domain) are filtered out of the Set-Cookie header when setting this cookie.</li>
</ul>
<p>Koa uses the <a href="https://github.com/jed/cookies">cookies</a> module where options are simply passed.</p>
<h3 id="ctxthrowstatus-msg-properties">ctx.throw([status], [msg], [properties])</h3>
<p>Helper method to throw an error with a <code>.status</code> property defaulting to <code>500</code> that will allow Koa to respond appropriately. The following combinations are allowed:</p>
<div class="sourceCode" id="cb3"><pre class="sourceCode js"><code class="sourceCode javascript"><a class="sourceLine" id="cb3-1" data-line-number="1"><span class="va">ctx</span>.<span class="at">throw</span>(<span class="dv">400</span>)<span class="op">;</span></a>
<a class="sourceLine" id="cb3-2" data-line-number="2"><span class="va">ctx</span>.<span class="at">throw</span>(<span class="dv">400</span><span class="op">,</span> <span class="st">&#39;name required&#39;</span>)<span class="op">;</span></a>
<a class="sourceLine" id="cb3-3" data-line-number="3"><span class="va">ctx</span>.<span class="at">throw</span>(<span class="dv">400</span><span class="op">,</span> <span class="st">&#39;name required&#39;</span><span class="op">,</span> <span class="op">{</span> <span class="dt">user</span><span class="op">:</span> user <span class="op">}</span>)<span class="op">;</span></a></code></pre></div>
<p>For example <code>ctx.throw(400, &#39;name required&#39;)</code> is equivalent to:</p>
<div class="sourceCode" id="cb4"><pre class="sourceCode js"><code class="sourceCode javascript"><a class="sourceLine" id="cb4-1" data-line-number="1"><span class="kw">const</span> err <span class="op">=</span> <span class="kw">new</span> <span class="at">Error</span>(<span class="st">&#39;name required&#39;</span>)<span class="op">;</span></a>
<a class="sourceLine" id="cb4-2" data-line-number="2"><span class="va">err</span>.<span class="at">status</span> <span class="op">=</span> <span class="dv">400</span><span class="op">;</span></a>
<a class="sourceLine" id="cb4-3" data-line-number="3"><span class="va">err</span>.<span class="at">expose</span> <span class="op">=</span> <span class="kw">true</span><span class="op">;</span></a>
<a class="sourceLine" id="cb4-4" data-line-number="4"><span class="cf">throw</span> err<span class="op">;</span></a></code></pre></div>
<p>Note that these are user-level errors and are flagged with <code>err.expose</code> meaning the messages are appropriate for client responses, which is typically not the case for error messages since you do not want to leak failure details.</p>
<p>You may optionally pass a <code>properties</code> object which is merged into the error as-is, useful for decorating machine-friendly errors which are reported to the requester upstream.</p>
<div class="sourceCode" id="cb5"><pre class="sourceCode js"><code class="sourceCode javascript"><a class="sourceLine" id="cb5-1" data-line-number="1"><span class="va">ctx</span>.<span class="at">throw</span>(<span class="dv">401</span><span class="op">,</span> <span class="st">&#39;access_denied&#39;</span><span class="op">,</span> <span class="op">{</span> <span class="dt">user</span><span class="op">:</span> user <span class="op">}</span>)<span class="op">;</span></a></code></pre></div>
<p>Koa uses <a href="https://github.com/jshttp/http-errors">http-errors</a> to create errors.</p>
<h3 id="ctxassertvalue-status-msg-properties">ctx.assert(value, [status], [msg], [properties])</h3>
<p>Helper method to throw an error similar to <code>.throw()</code> when <code>!value</code>. Similar to node&#39;s <a href="http://nodejs.org/api/assert.html">assert()</a> method.</p>
<div class="sourceCode" id="cb6"><pre class="sourceCode js"><code class="sourceCode javascript"><a class="sourceLine" id="cb6-1" data-line-number="1"><span class="va">ctx</span>.<span class="at">assert</span>(<span class="va">ctx</span>.<span class="va">state</span>.<span class="at">user</span><span class="op">,</span> <span class="dv">401</span><span class="op">,</span> <span class="st">&#39;User not found. Please login!&#39;</span>)<span class="op">;</span></a></code></pre></div>
<p>Koa uses <a href="https://github.com/jshttp/http-assert">http-assert</a> for assertions.</p>
<h3 id="ctxrespond">ctx.respond</h3>
<p>To bypass Koa&#39;s built-in response handling, you may explicitly set <code>ctx.respond = false;</code>. Use this if you want to write to the raw <code>res</code> object instead of letting Koa handle the response for you.</p>
<p>Note that using this is <strong>not</strong> supported by Koa. This may break intended functionality of Koa middleware and Koa itself. Using this property is considered a hack and is only a convenience to those wishing to use traditional <code>fn(req, res)</code> functions and middleware within Koa.</p>
<h2 id="request-aliases">Request aliases</h2>
<p>The following accessors and alias <a href="request.md">Request</a> equivalents:</p>
<ul>
<li><code>ctx.header</code></li>
<li><code>ctx.headers</code></li>
<li><code>ctx.method</code></li>
<li><code>ctx.method=</code></li>
<li><code>ctx.url</code></li>
<li><code>ctx.url=</code></li>
<li><code>ctx.originalUrl</code></li>
<li><code>ctx.origin</code></li>
<li><code>ctx.href</code></li>
<li><code>ctx.path</code></li>
<li><code>ctx.path=</code></li>
<li><code>ctx.query</code></li>
<li><code>ctx.query=</code></li>
<li><code>ctx.querystring</code></li>
<li><code>ctx.querystring=</code></li>
<li><code>ctx.host</code></li>
<li><code>ctx.hostname</code></li>
<li><code>ctx.fresh</code></li>
<li><code>ctx.stale</code></li>
<li><code>ctx.socket</code></li>
<li><code>ctx.protocol</code></li>
<li><code>ctx.secure</code></li>
<li><code>ctx.ip</code></li>
<li><code>ctx.ips</code></li>
<li><code>ctx.subdomains</code></li>
<li><code>ctx.is()</code></li>
<li><code>ctx.accepts()</code></li>
<li><code>ctx.acceptsEncodings()</code></li>
<li><code>ctx.acceptsCharsets()</code></li>
<li><code>ctx.acceptsLanguages()</code></li>
<li><code>ctx.get()</code></li>
</ul>
<h2 id="response-aliases">Response aliases</h2>
<p>The following accessors and alias <a href="response.md">Response</a> equivalents:</p>
<ul>
<li><code>ctx.body</code></li>
<li><code>ctx.body=</code></li>
<li><code>ctx.status</code></li>
<li><code>ctx.status=</code></li>
<li><code>ctx.message</code></li>
<li><code>ctx.message=</code></li>
<li><code>ctx.length=</code></li>
<li><code>ctx.length</code></li>
<li><code>ctx.type=</code></li>
<li><code>ctx.type</code></li>
<li><code>ctx.headerSent</code></li>
<li><code>ctx.redirect()</code></li>
<li><code>ctx.attachment()</code></li>
<li><code>ctx.set()</code></li>
<li><code>ctx.append()</code></li>
<li><code>ctx.remove()</code></li>
<li><code>ctx.lastModified=</code></li>
<li><code>ctx.etag=</code></li>
</ul>
</body></html>