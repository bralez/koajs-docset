# Build script for Dash docset

## Useful links

[Koajs online documentation](http://koajs.com)

[Docset generation guide](https://kapeli.com/docsets)

## Requirements

* [Pandoc](http://pandoc.org)
* [Dashing](https://github.com/technosophos/dashing)

### Installation on OSX

```
brew install pandoc && \
brew install dashing
```

## Usage

From command shell:
```
$ ./generate-docset.sh
```