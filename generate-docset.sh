#!/usr/bin/env bash

curl https://raw.githubusercontent.com/koajs/koa/master/docs/api/index.md | pandoc -f gfm > ./HTML/index.html
curl https://raw.githubusercontent.com/koajs/koa/master/docs/api/context.md | pandoc -f gfm > ./HTML/context.html
curl https://raw.githubusercontent.com/koajs/koa/master/docs/api/request.md | pandoc -f gfm > ./HTML/request.html
curl https://raw.githubusercontent.com/koajs/koa/master/docs/api/response.md | pandoc -f gfm > ./HTML/response.html

dashing build --source ./HTML arangojs