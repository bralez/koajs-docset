<h1 id="response">Response</h1>
<p>A Koa <code>Response</code> object is an abstraction on top of node's vanilla response object, providing additional functionality that is useful for every day HTTP server development.</p>
<h2 id="api">API</h2>
<h3 id="responseheader">response.header</h3>
<p>Response header object.</p>
<h3 id="responseheaders">response.headers</h3>
<p>Response header object. Alias as <code>response.header</code>.</p>
<h3 id="responsesocket">response.socket</h3>
<p>Request socket.</p>
<h3 id="responsestatus">response.status</h3>
<p>Get response status. By default, <code>response.status</code> is set to <code>404</code> unlike node's <code>res.statusCode</code> which defaults to <code>200</code>.</p>
<h3 id="responsestatus-1">response.status=</h3>
<p>Set response status via numeric code:</p>
<ul>
<li>100 &quot;continue&quot;</li>
<li>101 &quot;switching protocols&quot;</li>
<li>102 &quot;processing&quot;</li>
<li>200 &quot;ok&quot;</li>
<li>201 &quot;created&quot;</li>
<li>202 &quot;accepted&quot;</li>
<li>203 &quot;non-authoritative information&quot;</li>
<li>204 &quot;no content&quot;</li>
<li>205 &quot;reset content&quot;</li>
<li>206 &quot;partial content&quot;</li>
<li>207 &quot;multi-status&quot;</li>
<li>208 &quot;already reported&quot;</li>
<li>226 &quot;im used&quot;</li>
<li>300 &quot;multiple choices&quot;</li>
<li>301 &quot;moved permanently&quot;</li>
<li>302 &quot;found&quot;</li>
<li>303 &quot;see other&quot;</li>
<li>304 &quot;not modified&quot;</li>
<li>305 &quot;use proxy&quot;</li>
<li>307 &quot;temporary redirect&quot;</li>
<li>308 &quot;permanent redirect&quot;</li>
<li>400 &quot;bad request&quot;</li>
<li>401 &quot;unauthorized&quot;</li>
<li>402 &quot;payment required&quot;</li>
<li>403 &quot;forbidden&quot;</li>
<li>404 &quot;not found&quot;</li>
<li>405 &quot;method not allowed&quot;</li>
<li>406 &quot;not acceptable&quot;</li>
<li>407 &quot;proxy authentication required&quot;</li>
<li>408 &quot;request timeout&quot;</li>
<li>409 &quot;conflict&quot;</li>
<li>410 &quot;gone&quot;</li>
<li>411 &quot;length required&quot;</li>
<li>412 &quot;precondition failed&quot;</li>
<li>413 &quot;payload too large&quot;</li>
<li>414 &quot;uri too long&quot;</li>
<li>415 &quot;unsupported media type&quot;</li>
<li>416 &quot;range not satisfiable&quot;</li>
<li>417 &quot;expectation failed&quot;</li>
<li>418 &quot;I'm a teapot&quot;</li>
<li>422 &quot;unprocessable entity&quot;</li>
<li>423 &quot;locked&quot;</li>
<li>424 &quot;failed dependency&quot;</li>
<li>426 &quot;upgrade required&quot;</li>
<li>428 &quot;precondition required&quot;</li>
<li>429 &quot;too many requests&quot;</li>
<li>431 &quot;request header fields too large&quot;</li>
<li>500 &quot;internal server error&quot;</li>
<li>501 &quot;not implemented&quot;</li>
<li>502 &quot;bad gateway&quot;</li>
<li>503 &quot;service unavailable&quot;</li>
<li>504 &quot;gateway timeout&quot;</li>
<li>505 &quot;http version not supported&quot;</li>
<li>506 &quot;variant also negotiates&quot;</li>
<li>507 &quot;insufficient storage&quot;</li>
<li>508 &quot;loop detected&quot;</li>
<li>510 &quot;not extended&quot;</li>
<li>511 &quot;network authentication required&quot;</li>
</ul>
<p><strong>NOTE</strong>: don't worry too much about memorizing these strings, if you have a typo an error will be thrown, displaying this list so you can make a correction.</p>
<h3 id="responsemessage">response.message</h3>
<p>Get response status message. By default, <code>response.message</code> is associated with <code>response.status</code>.</p>
<h3 id="responsemessage-1">response.message=</h3>
<p>Set response status message to the given value.</p>
<h3 id="responselength">response.length=</h3>
<p>Set response Content-Length to the given value.</p>
<h3 id="responselength-1">response.length</h3>
<p>Return response Content-Length as a number when present, or deduce from <code>ctx.body</code> when possible, or <code>undefined</code>.</p>
<h3 id="responsebody">response.body</h3>
<p>Get response body.</p>
<h3 id="responsebody-1">response.body=</h3>
<p>Set response body to one of the following:</p>
<ul>
<li><code>string</code> written</li>
<li><code>Buffer</code> written</li>
<li><code>Stream</code> piped</li>
<li><code>Object</code> || <code>Array</code> json-stringified</li>
<li><code>null</code> no content response</li>
</ul>
<p>If <code>response.status</code> has not been set, Koa will automatically set the status to <code>200</code> or <code>204</code>.</p>
<h4 id="string">String</h4>
<p>The Content-Type is defaulted to text/html or text/plain, both with a default charset of utf-8. The Content-Length field is also set.</p>
<h4 id="buffer">Buffer</h4>
<p>The Content-Type is defaulted to application/octet-stream, and Content-Length is also set.</p>
<h4 id="stream">Stream</h4>
<p>The Content-Type is defaulted to application/octet-stream.</p>
<p>Whenever a stream is set as the response body, <code>.onerror</code> is automatically added as a listener to the <code>error</code> event to catch any errors. In addition, whenever the request is closed (even prematurely), the stream is destroyed. If you do not want these two features, do not set the stream as the body directly. For example, you may not want this when setting the body as an HTTP stream in a proxy as it would destroy the underlying connection.</p>
<p>See: <a href="https://github.com/koajs/koa/pull/612" class="uri">https://github.com/koajs/koa/pull/612</a> for more information.</p>
<p>Here's an example of stream error handling without automatically destroying the stream:</p>
<div class="sourceCode" id="cb1"><pre class="sourceCode js"><code class="sourceCode javascript"><a class="sourceLine" id="cb1-1" data-line-number="1"><span class="kw">const</span> PassThrough <span class="op">=</span> <span class="at">require</span>(<span class="st">&#39;stream&#39;</span>).<span class="at">PassThrough</span><span class="op">;</span></a>
<a class="sourceLine" id="cb1-2" data-line-number="2"></a>
<a class="sourceLine" id="cb1-3" data-line-number="3"><span class="va">app</span>.<span class="at">use</span>(async ctx <span class="op">=&gt;</span> <span class="op">{</span></a>
<a class="sourceLine" id="cb1-4" data-line-number="4">  <span class="va">ctx</span>.<span class="at">body</span> <span class="op">=</span> <span class="va">someHTTPStream</span>.<span class="at">on</span>(<span class="st">&#39;error&#39;</span><span class="op">,</span> <span class="va">ctx</span>.<span class="at">onerror</span>).<span class="at">pipe</span>(<span class="at">PassThrough</span>())<span class="op">;</span></a>
<a class="sourceLine" id="cb1-5" data-line-number="5"><span class="op">}</span>)<span class="op">;</span></a></code></pre></div>
<h4 id="object">Object</h4>
<p>The Content-Type is defaulted to application/json. This includes plain objects <code>{ foo: 'bar' }</code> and arrays <code>['foo', 'bar']</code>.</p>
<h3 id="responsegetfield">response.get(field)</h3>
<p>Get a response header field value with case-insensitive <code>field</code>.</p>
<div class="sourceCode" id="cb2"><pre class="sourceCode js"><code class="sourceCode javascript"><a class="sourceLine" id="cb2-1" data-line-number="1"><span class="kw">const</span> etag <span class="op">=</span> <span class="va">ctx</span>.<span class="va">response</span>.<span class="at">get</span>(<span class="st">&#39;ETag&#39;</span>)<span class="op">;</span></a></code></pre></div>
<h3 id="responsesetfield-value">response.set(field, value)</h3>
<p>Set response header <code>field</code> to <code>value</code>:</p>
<div class="sourceCode" id="cb3"><pre class="sourceCode js"><code class="sourceCode javascript"><a class="sourceLine" id="cb3-1" data-line-number="1"><span class="va">ctx</span>.<span class="at">set</span>(<span class="st">&#39;Cache-Control&#39;</span><span class="op">,</span> <span class="st">&#39;no-cache&#39;</span>)<span class="op">;</span></a></code></pre></div>
<h3 id="responseappendfield-value">response.append(field, value)</h3>
<p>Append additional header <code>field</code> with value <code>val</code>.</p>
<div class="sourceCode" id="cb4"><pre class="sourceCode js"><code class="sourceCode javascript"><a class="sourceLine" id="cb4-1" data-line-number="1"><span class="va">ctx</span>.<span class="at">append</span>(<span class="st">&#39;Link&#39;</span><span class="op">,</span> <span class="st">&#39;&lt;http://127.0.0.1/&gt;&#39;</span>)<span class="op">;</span></a></code></pre></div>
<h3 id="responsesetfields">response.set(fields)</h3>
<p>Set several response header <code>fields</code> with an object:</p>
<div class="sourceCode" id="cb5"><pre class="sourceCode js"><code class="sourceCode javascript"><a class="sourceLine" id="cb5-1" data-line-number="1"><span class="va">ctx</span>.<span class="at">set</span>(<span class="op">{</span></a>
<a class="sourceLine" id="cb5-2" data-line-number="2">  <span class="st">&#39;Etag&#39;</span><span class="op">:</span> <span class="st">&#39;1234&#39;</span><span class="op">,</span></a>
<a class="sourceLine" id="cb5-3" data-line-number="3">  <span class="st">&#39;Last-Modified&#39;</span><span class="op">:</span> date</a>
<a class="sourceLine" id="cb5-4" data-line-number="4"><span class="op">}</span>)<span class="op">;</span></a></code></pre></div>
<h3 id="responseremovefield">response.remove(field)</h3>
<p>Remove header <code>field</code>.</p>
<h3 id="responsetype">response.type</h3>
<p>Get response <code>Content-Type</code> void of parameters such as &quot;charset&quot;.</p>
<div class="sourceCode" id="cb6"><pre class="sourceCode js"><code class="sourceCode javascript"><a class="sourceLine" id="cb6-1" data-line-number="1"><span class="kw">const</span> ct <span class="op">=</span> <span class="va">ctx</span>.<span class="at">type</span><span class="op">;</span></a>
<a class="sourceLine" id="cb6-2" data-line-number="2"><span class="co">// =&gt; &quot;image/png&quot;</span></a></code></pre></div>
<h3 id="responsetype-1">response.type=</h3>
<p>Set response <code>Content-Type</code> via mime string or file extension.</p>
<div class="sourceCode" id="cb7"><pre class="sourceCode js"><code class="sourceCode javascript"><a class="sourceLine" id="cb7-1" data-line-number="1"><span class="va">ctx</span>.<span class="at">type</span> <span class="op">=</span> <span class="st">&#39;text/plain; charset=utf-8&#39;</span><span class="op">;</span></a>
<a class="sourceLine" id="cb7-2" data-line-number="2"><span class="va">ctx</span>.<span class="at">type</span> <span class="op">=</span> <span class="st">&#39;image/png&#39;</span><span class="op">;</span></a>
<a class="sourceLine" id="cb7-3" data-line-number="3"><span class="va">ctx</span>.<span class="at">type</span> <span class="op">=</span> <span class="st">&#39;.png&#39;</span><span class="op">;</span></a>
<a class="sourceLine" id="cb7-4" data-line-number="4"><span class="va">ctx</span>.<span class="at">type</span> <span class="op">=</span> <span class="st">&#39;png&#39;</span><span class="op">;</span></a></code></pre></div>
<p>Note: when appropriate a <code>charset</code> is selected for you, for example <code>response.type = 'html'</code> will default to &quot;utf-8&quot;. If you need to overwrite <code>charset</code>, use <code>ctx.set('Content-Type', 'text/html')</code> to set response header field to value directly.</p>
<h3 id="responseistypes">response.is(types...)</h3>
<p>Very similar to <code>ctx.request.is()</code>. Check whether the response type is one of the supplied types. This is particularly useful for creating middleware that manipulate responses.</p>
<p>For example, this is a middleware that minifies all HTML responses except for streams.</p>
<div class="sourceCode" id="cb8"><pre class="sourceCode js"><code class="sourceCode javascript"><a class="sourceLine" id="cb8-1" data-line-number="1"><span class="kw">const</span> minify <span class="op">=</span> <span class="at">require</span>(<span class="st">&#39;html-minifier&#39;</span>)<span class="op">;</span></a>
<a class="sourceLine" id="cb8-2" data-line-number="2"></a>
<a class="sourceLine" id="cb8-3" data-line-number="3"><span class="va">app</span>.<span class="at">use</span>(<span class="at">async</span> (ctx<span class="op">,</span> next) <span class="op">=&gt;</span> <span class="op">{</span></a>
<a class="sourceLine" id="cb8-4" data-line-number="4">  await <span class="at">next</span>()<span class="op">;</span></a>
<a class="sourceLine" id="cb8-5" data-line-number="5"></a>
<a class="sourceLine" id="cb8-6" data-line-number="6">  <span class="cf">if</span> (<span class="op">!</span><span class="va">ctx</span>.<span class="va">response</span>.<span class="at">is</span>(<span class="st">&#39;html&#39;</span>)) <span class="cf">return</span><span class="op">;</span></a>
<a class="sourceLine" id="cb8-7" data-line-number="7"></a>
<a class="sourceLine" id="cb8-8" data-line-number="8">  <span class="kw">let</span> body <span class="op">=</span> <span class="va">ctx</span>.<span class="at">body</span><span class="op">;</span></a>
<a class="sourceLine" id="cb8-9" data-line-number="9">  <span class="cf">if</span> (<span class="op">!</span>body <span class="op">||</span> <span class="va">body</span>.<span class="at">pipe</span>) <span class="cf">return</span><span class="op">;</span></a>
<a class="sourceLine" id="cb8-10" data-line-number="10"></a>
<a class="sourceLine" id="cb8-11" data-line-number="11">  <span class="cf">if</span> (<span class="va">Buffer</span>.<span class="at">isBuffer</span>(body)) body <span class="op">=</span> <span class="va">body</span>.<span class="at">toString</span>()<span class="op">;</span></a>
<a class="sourceLine" id="cb8-12" data-line-number="12">  <span class="va">ctx</span>.<span class="at">body</span> <span class="op">=</span> <span class="at">minify</span>(body)<span class="op">;</span></a>
<a class="sourceLine" id="cb8-13" data-line-number="13"><span class="op">}</span>)<span class="op">;</span></a></code></pre></div>
<h3 id="responseredirecturl-alt">response.redirect(url, [alt])</h3>
<p>Perform a [302] redirect to <code>url</code>.</p>
<p>The string &quot;back&quot; is special-cased to provide Referrer support, when Referrer is not present <code>alt</code> or &quot;/&quot; is used.</p>
<div class="sourceCode" id="cb9"><pre class="sourceCode js"><code class="sourceCode javascript"><a class="sourceLine" id="cb9-1" data-line-number="1"><span class="va">ctx</span>.<span class="at">redirect</span>(<span class="st">&#39;back&#39;</span>)<span class="op">;</span></a>
<a class="sourceLine" id="cb9-2" data-line-number="2"><span class="va">ctx</span>.<span class="at">redirect</span>(<span class="st">&#39;back&#39;</span><span class="op">,</span> <span class="st">&#39;/index.html&#39;</span>)<span class="op">;</span></a>
<a class="sourceLine" id="cb9-3" data-line-number="3"><span class="va">ctx</span>.<span class="at">redirect</span>(<span class="st">&#39;/login&#39;</span>)<span class="op">;</span></a>
<a class="sourceLine" id="cb9-4" data-line-number="4"><span class="va">ctx</span>.<span class="at">redirect</span>(<span class="st">&#39;http://google.com&#39;</span>)<span class="op">;</span></a></code></pre></div>
<p>To alter the default status of <code>302</code>, simply assign the status before or after this call. To alter the body, assign it after this call:</p>
<div class="sourceCode" id="cb10"><pre class="sourceCode js"><code class="sourceCode javascript"><a class="sourceLine" id="cb10-1" data-line-number="1"><span class="va">ctx</span>.<span class="at">status</span> <span class="op">=</span> <span class="dv">301</span><span class="op">;</span></a>
<a class="sourceLine" id="cb10-2" data-line-number="2"><span class="va">ctx</span>.<span class="at">redirect</span>(<span class="st">&#39;/cart&#39;</span>)<span class="op">;</span></a>
<a class="sourceLine" id="cb10-3" data-line-number="3"><span class="va">ctx</span>.<span class="at">body</span> <span class="op">=</span> <span class="st">&#39;Redirecting to shopping cart&#39;</span><span class="op">;</span></a></code></pre></div>
<h3 id="responseattachmentfilename">response.attachment([filename])</h3>
<p>Set <code>Content-Disposition</code> to &quot;attachment&quot; to signal the client to prompt for download. Optionally specify the <code>filename</code> of the download.</p>
<h3 id="responseheadersent">response.headerSent</h3>
<p>Check if a response header has already been sent. Useful for seeing if the client may be notified on error.</p>
<h3 id="responselastmodified">response.lastModified</h3>
<p>Return the <code>Last-Modified</code> header as a <code>Date</code>, if it exists.</p>
<h3 id="responselastmodified-1">response.lastModified=</h3>
<p>Set the <code>Last-Modified</code> header as an appropriate UTC string. You can either set it as a <code>Date</code> or date string.</p>
<div class="sourceCode" id="cb11"><pre class="sourceCode js"><code class="sourceCode javascript"><a class="sourceLine" id="cb11-1" data-line-number="1"><span class="va">ctx</span>.<span class="va">response</span>.<span class="at">lastModified</span> <span class="op">=</span> <span class="kw">new</span> <span class="at">Date</span>()<span class="op">;</span></a></code></pre></div>
<h3 id="responseetag">response.etag=</h3>
<p>Set the ETag of a response including the wrapped <code>&quot;</code>s. Note that there is no corresponding <code>response.etag</code> getter.</p>
<div class="sourceCode" id="cb12"><pre class="sourceCode js"><code class="sourceCode javascript"><a class="sourceLine" id="cb12-1" data-line-number="1"><span class="va">ctx</span>.<span class="va">response</span>.<span class="at">etag</span> <span class="op">=</span> <span class="va">crypto</span>.<span class="at">createHash</span>(<span class="st">&#39;md5&#39;</span>).<span class="at">update</span>(<span class="va">ctx</span>.<span class="at">body</span>).<span class="at">digest</span>(<span class="st">&#39;hex&#39;</span>)<span class="op">;</span></a></code></pre></div>
<h3 id="responsevaryfield">response.vary(field)</h3>
<p>Vary on <code>field</code>.</p>
<h3 id="responseflushheaders">response.flushHeaders()</h3>
<p>Flush any set headers, and begin the body.</p>
